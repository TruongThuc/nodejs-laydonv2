module.exports = {
    apps : [
        {
          name: "extentionsAPI",
          script: "./index.js",
          watch: true,
          ignore_watch : ["node_modules", "app/logs"],
          env: {
            "PORT": 3000,
            "NODE_ENV": "development",
          }
        }
    ]
  }


  