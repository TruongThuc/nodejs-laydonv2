//
const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000
const bodyParser = require('body-parser')
const session = require('express-session')
const apiRouters = require('./app/routers/apiRouters')


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support url encoded bodies
app.use(session({
  // genid: function(req) {
  //   return genuuid() 
  // },
  secret: 'key la bi mat',
  resave: true,
  saveUninitialized: true,
  maxAge: 60*60*1000   // 1 hour
}))

apiRouters(app)

//
app.listen(port)
console.log('Server started on port: ' + port + ' at '+ new Date())
