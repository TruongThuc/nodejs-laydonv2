----------- Cài đặt ------------
1/ cài nodeJS >10.x (https://nodejs.org/en/) nên dùng bản LTS
2/ (optional) cài pm2 để quản lý task, xem log server (https://pm2.keymetrics.io/)
3/ chạy file init.command
    - chờ cài các modun cần thiết

----------- Kiểm tra các bước cài đặt ------------
(1) gõ lệnh nodejs -v && npm -v
(2) pm2 -v

----------- Cách sử dụng ------------
trong folder chứa 2 file các file .command
    - Nếu đã có pm2 thì chạy file : pm2.command
    - Nếu chưa có thì chạy file : nodejs.command

