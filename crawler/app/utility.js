
// this file only use in browser support ES6 module

module.exports.mutationWatching = (HTML_Element, callback)  => {
    console.log(document.body.contains(HTML_Element));
    console.log(!!HTML_Element, HTML_Element);

    let mutationObserver = new MutationObserver(function(mutations, ob) {
        callback(mutations, ob);
    });

    // Starts listening for changes in the root HTML element of the page.
    mutationObserver.observe(HTML_Element, {
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
    });
}
//bắt sự kiện nội bộ thẻ HTML thay đổi

module.exports.getUrlParameter =  (name, location)  => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.href.split(location.host)[1]);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
//lấy queryString phiên bản ngu

module.exports.addCommas = (nStr) =>{
    nStr += '';
    let x = nStr.split('.');
    let x1 = x[0];
    let x2 = x.length > 1 ? '.' + x[1] : '';
    let rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
//addCommas(50000); // 50,000

module.exports.toTitleCase = (str) =>{
    return str.toLowerCase().split(' ').map(function(word) {
        return word.replace(word[0], word[0].toUpperCase());
    }).join(' ');
}
//toTitleCase("mY NAME is SomeThing WeIrD"); // My Name Is Something Weird

module.exports.linkify =  (content) => {
    var replacedText, replacePattern1, replacePattern2;
 
    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = content.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
 
    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
 
    return replacedText;
}
//linkify('https://mona.media'); //<a href="https://mona.media" target="_blank">https://mona.media</a>
