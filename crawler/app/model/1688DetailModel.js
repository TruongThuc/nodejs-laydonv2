const  puppeteer  = require('puppeteer')

module.exports = async function(page){
    let data = null
    try {
        let buyerValue = ''
        try {
            buyerValue = await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let buyerLiArray = orderDetail.querySelector(".unit-buyer-info").querySelectorAll('ul > li');
                buyerLiArray = Array.from(buyerLiArray);
                return {
                    id: buyerLiArray[0].textContent.split('订单号：')[1].trim(),
                    idAlipay : buyerLiArray[1].textContent.split('支付宝交易号：')[1].trim(),
                    codebill : buyerLiArray[2].textContent.split('收货人：')[1].trim(),
                    address : buyerLiArray[3].textContent.split('收货地址：')[1].trim(),
                    phone : buyerLiArray[4].textContent.split('手机：')[1].trim(),
                    phone2 : buyerLiArray[5].textContent.split('电话：')[1].trim()
                }
            })
        } catch (error) {
            console.log('buyerValue', error);

        }
        let sellerValue = ''
        try {
            sellerValue = await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let sellerLiArray = orderDetail.querySelector(".unit-seller-info").querySelectorAll('ul > li');
                sellerLiArray = Array.from(sellerLiArray); 
                return {
                    supplier :  [sellerLiArray[0].querySelector('a[data-tracelog="viewUserCompany"]').textContent.trim(), 
                    sellerLiArray[0].querySelector('a[data-tracelog="viewUserCompany"]').getAttribute('href')],
                    user : [sellerLiArray[1].querySelector('a[data-tracelog="viewUserProfile"]').textContent.trim(), 
                    sellerLiArray[1].querySelector('a[data-tracelog="viewUserProfile"]').getAttribute('href')],
                    userAlipay: sellerLiArray[2].textContent.split('支付宝账户：')[1].trim(),
                    phone : sellerLiArray[3].textContent.split('手机：')[1].trim(),
                    phone2 : sellerLiArray[4].textContent.split('电话：')[1].trim()
                }
            })
        } catch (error) {
            console.log('sellerValue', error);
        }
        let subtotalValue = ''
        try {
            subtotalValue =  await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let subtt = orderDetail.querySelector('.unit-order-list .total-cell');
                if(!subtt) {
                    subtt = orderDetail.querySelector('.unit-order-list .first-item > td:last-child');
                }
                return subtt.textContent.replace(/(\r\n|\n|\t|\r)/gm,"").trim()
            })
        } catch (error) {
            console.log(subtotalValue, error);
        }
        let shippingPriceValue = ''
        try {
            shippingPriceValue = await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let shipping = orderDetail.querySelector('.unit-order-total .label-line');
                return shipping.textContent.trim().split('运费：')[1].trim()
            })
        } catch (error) {
            console.log('shippingPriceValue', error)
        }
        let totalValue = ''
        try {
            totalValue = await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let total = orderDetail.querySelector('.unit-order-total .total-price');
                return total.textContent.trim()
            })
        } catch (error) {
            console.log('totalValue', error);
        }
        let productListValue = []
        try {
            productListValue = await page.evaluate(() => {
                let orderDetail = document.querySelector(".submod.submod-order-info");
                let productItemArray = orderDetail.querySelectorAll('.unit-order-list tbody .item');
                productItemArray = Array.from(productItemArray).map(item => {
                    console.log(item);
                    return {
                        imgSRC: item.querySelector('.cell-thumbnail img').getAttribute('src'),
                        pdName: item.querySelector('.cell-thumbnail .offer-title').textContent.trim(),
                        pdPrice: item.querySelectorAll('td')[1].textContent.trim(),
                        quantity: item.querySelectorAll('td')[2].textContent.trim()
                    }
                });
                return productItemArray
            })
        } catch (error) {
            console.log('productListValue', error);
        }
        let logisticsValue = null;
        try {
            const logisticsTabBtn = await page.$('#logisticsTabTitle');
            await logisticsTabBtn.click({ delay: 20, })
            await page.waitForSelector(".submod.submod-logistics-info",{visible: true})
            await autoScrollBtm(page)
            await page.waitForSelector(".submod.submod-logistics-info .logistics-item .logistics-flow-item.first-date", {visible: true})
            await page.waitFor(2000)
            logisticsValue= await page.evaluate( async () => {
                let logisticsArray =  document.querySelectorAll('.submod.submod-logistics-info .logistics-item');
                console.log("logisticsArray", logisticsArray);
                try {
                    logisticsArray = Array.from(logisticsArray).map(item => {
                        let lId = item.querySelectorAll('.info-item')[0].querySelector('.info-item-val').textContent;
                        let lCompany = item.querySelectorAll('.info-item')[1].querySelector('.info-item-val').textContent;
                        let lMailNumber = item.querySelectorAll('.info-item')[2].querySelector('.info-item-val').textContent;
                        let lShipTime = item.querySelectorAll('.info-item')[3].querySelector('.info-item-val').textContent;
                        let lFlowArray = item.querySelectorAll('.logistics-flow .logistics-flow-item');
                        // let lStatus  = item.querySelectorAll('.item-header .type')[0].textContent.trim();
                        let lFlow = [];
                        
                        Array.from(lFlowArray).length > 0 && Array.from(lFlowArray).map((fItem, index) => {
                            if(fItem.classList.contains('first-date')){
                                lFlow.push({
                                    date:  fItem.querySelector('.date').textContent.trim(),
                                    detail: [{
                                        time: fItem.querySelector('.time').textContent.trim(),
                                        area: fItem.querySelector('.area-name').textContent.trim(),
                                        remark: fItem.querySelector('.remark').textContent.trim()
                                    } ]
                                });
                                    
                            } else {
                                lFlow[lFlow.length - 1].detail.push({
                                    time: fItem.querySelector('.time').textContent.trim(),
                                    area: fItem.querySelector('.area-name').textContent.trim(),
                                    remark: fItem.querySelector('.remark').textContent.trim()
                                });
                            }
                        });
                        
                        console.log(lFlow);  
                        let lProducts = item.querySelector('.unit-logistics-prodlist .logistics-prodlist').querySelectorAll('.prod-item')
                        lProducts =  Array.from(lProducts).map(pdItem =>{
                            let sgimg = pdItem.querySelector('.a-img img');
                            return {
                                img: sgimg.getAttribute('src'), 
                                title: sgimg.getAttribute('alt'),
                            }
                        })
                        console.log(lProducts);
                        return {
                            id: lId.replace(/(\r\n|\n|\t|\r)/gm,"").trim(),
                            company:  lCompany.replace(/(\r\n|\n|\t|\r)/gm,"").trim(),
                            mailNumber: lMailNumber.replace(/(\r\n|\n|\t|\r)/gm,"").trim(),
                            shipTime: lShipTime.replace(/(\r\n|\n|\t|\r)/gm,"").trim(),
                            flow: lFlow,
                            products: lProducts,
                        }
                    });
                    return logisticsArray;
                } catch (error) {
                    console.log("logisticsValue error", error);
                    return [];
                }  
            })
        } catch (error) {
            console.log("logisticsValue", error);
            logisticsValue = [];
        }
       
        let isCancel = false;
        try {
            await page.waitForSelector(".stage-ct", {visible: true});
            
        } catch (error) {
            isCancel = true;
        }
        data = {
            detail: {
                buyer: buyerValue,
                seller: sellerValue,
                subtotal: subtotalValue,
                total: totalValue,
                productList: productListValue,
                shippingPrice: shippingPriceValue,
            },
            isCancel: isCancel,
            type: 3,
            logistics: logisticsValue,
            contract: {}
        };
     
    } catch (error) {

        console.log("Modal 1688 Error", error);
        data = null
    }
    return data
}
async function autoScrollBtm(page){
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            let totalHeight = 0;
            let distance = 100;
            let timer = setInterval(() => {
                let scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

