
module.exports = async function(pageDetail){
    let data = null
    try {
        // data = await pageDetail.evaluate(() => detailData)
        //                     data.orders.id = util.getUrlParameter("bizOrderId", location)
        data = await pageDetail.evaluate(() => detailData)
        data.type = 2
        try {
            await pageDetail.waitForSelector('.trade-detail-prompt', {timeout: 10000})
            const logicticsFlow = await pageDetail.evaluate(() => {
                let flowArray = [];
                const ulFlow = document.querySelector('.trade-detail-logistic');
                flowArray = Array.from(ulFlow.querySelectorAll('.logistic-detail')).map(item => {
                    return item.textContent
                });
                return flowArray;
            });
            data.logicticsFlow = logicticsFlow
        } catch (e) {    
            data.logicticsFlow = [] 
        }
        
    } catch (error) {
        console.log("Modal tb Error", error);
        data = null
    }
    
    return data
}