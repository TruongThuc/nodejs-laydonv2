


module.exports = async function(pageDetail){
    let data = null
    try {
        data = await pageDetail.evaluate(() => data)
        data.type = 1
        try {
            await pageDetail.waitForSelector('.logistics-info-mod__list___2KLt8', {timeout: 10000})
            const logicticsFlow = await pageDetail.evaluate(() => {
                let flowArray = [];
                const ulFlow = document.querySelector('.logistics-info-mod__list___2KLt8');
                flowArray = Array.from(ulFlow.querySelectorAll('li')).map(item => {
                    return item.textContent
                });
                return flowArray;
            });
            data.logicticsFlow = logicticsFlow
        } catch (e) {    
            data.logicticsFlow = [] 
        }
        
    } catch (error) {
        console.log("Modal tb Error", error);
        data = null
    }
    
    return data
}