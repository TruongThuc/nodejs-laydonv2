

const {DOtbLOGIN, DOgotoURL} = require('../crawler');
const tbdetail = require('../model/tbDetailModel');
const tmalldetail = require('../model/tmallDeailModel');
const _1688detail = require('../model/1688DetailModel');
const util = require('../utility');
const fs = require("fs"); 
const fspm = require("fs").promises;
const path = require('path');
// utility functions
// I/O json logs

const readJsonFile = filePath => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, jsonString) => {
            if (err) {
                
                if(err.code == 'ENOENT'){
                    resolve('');
                } else {
                    console.log("File read failed:", err);
                    reject(err);
                }
                
            } else {
                // console.log('File data:', jsonString) 
                resolve(jsonString);
            }
            
        })
    });
}
const writeJsonFile = (filePath, stringData) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, stringData , err => {
            if (err) {
                console.log("File write failed:", err)
                reject(false);
            }
        
            resolve(true);
        })
    });
}
const updateJsonFile = async (filePath, stringData) => {
    try {
        const data = await readJsonFile(filePath)
        // console.log('updateJsonFile - readJsonFile', data)
        let newData = null;
        if(!!data){
            newData = {...JSON.parse(data), ...JSON.parse(stringData)}
        } else {
            newData = JSON.parse(stringData);
        }
        const status = await writeJsonFile(filePath, JSON.stringify(newData))
        return status
    } catch (error) {
       
        console.log('updateJsonFile error', error)
        return false
    }
    
}
module.exports =  function(app) {
    let currentBrowser = {};
    try {
        app.route('/').get((req, res) => {
            res.send({
                time_exicuted: new Date(),
                status: true,
                message: "Hi, buddy! Welcom to extention api root router",
            });
        })
        
        app.route('/tblogin').get((req,res) => {
            
            if(req.session.username){
                res.send({ message: `Hi, buddy !!\nwhat are u going to do ... Bot has signin under ${ req.session.username }`, status: true})
            } else {
                res.send({ message: "Bot not signin yet !! Try back after signin", status: false})
            }
        }).post(async (req,res) => {
            try {
                //vmt1995 / v0minhthien
                let {username, password, is307} = req.body
                
                if( !username || !password ) {
                    
                    return res.send({
                        status: false, 
                        message: `POST param invalid: ${ !username ? "username missing" : "" } ${ !password ? "password missing" : "" }`,
                 
                    })
                }
                // if(req.session.username === username && req.session.password === password){
                //     if(currentBrowser[req.session.username]){
                //         return res.send({
                //             status: true,
                //             message: "success",
                //             data: {username: req.session.username,
                //                 browserId: req.session.id}
                //         })
                //     }
                // }
                
                    
                let rs = await DOtbLOGIN(username, password)
                if(rs.hasOwnProperty("error")) throw rs.error
                req.session.username = rs.user
                req.session.password = password
                currentBrowser[req.session.username] = rs.browser
                console.log("Login success your session is:",req.session.id);
          
                if(req.session.username){

                    res.send({
                        status: true,
                        message: "success",
                        data: {username: req.session.username,
                            browserId: req.session.id}
                    })
                } else {
                    res.send({
                        status: false, 
                        message: rs
                    })
                }
                
            } catch (error) {
                res.send({error: error})
            }   
        })
        app.route('/tblogout').post(async (req,res) => {
            req.logout();
            delete req.session.username 
            req.session.destroy()
            res.status(401).send({status: true, message: "You have been logged out."});
        })
        app.route('/tbfetchdetail').post(async (req,res) => {
           console.log('tbfetchdetail');
            if(req.session.username){
                console.log("tbfetchdetail with session:", req.session.id);
                console.log("tbfetchdetail with username", req.session.username);
                let { detailurl } = req.body
                !detailurl ?  res.send({
                    status: false, 
                    message: `POST param invalid: ${ !detailurl ? "detailurl missing" : "" }`,
                }) : ''
                try {
                    console.log("detailurl", detailurl)
                    const pageDetail = await DOgotoURL(currentBrowser[req.session.username], detailurl)
                    const location = await pageDetail.evaluate(() => location)
                    console.log("host", location.host)
                    let data = null
                    let detailmakeUp = null
                    switch (location.host) {
                        case "trade.tmall.com":
                            // data = await pageDetail.evaluate(() => detailData)
                            // data.orders.id = util.getUrlParameter("bizOrderId", location)
                            detailmakeUp = await tmalldetail(pageDetail)
                            detailmakeUp.orders.id = util.getUrlParameter("bizOrderId", location)
                            break;
                        case "trade.taobao.com":
                            detailmakeUp = await tbdetail(pageDetail)
                            break;
                        case "trade.1688.com":
                            detailmakeUp = await  _1688detail(pageDetail)
                            break;
                        case "login.taobao.com":
                            throw "Đăng nhập extention thất bại. Vui lòng thử lại"
                            break;
                        default:
                            // data = "hostname invalid: try again later."
                            throw "URL invalid"
                            break;
                    }
                    pageDetail.close()
                    if(!!detailmakeUp){
                        res.send({
                            status: true,
                            message: "",
                            data: detailmakeUp,
                        })
                    } else {
                        res.send({
                            error: "datanull",
                        })
                    }
                     
                } catch (error) {
                    console.log(error)
                    res.send({error: error})
                }
            } else {
                res.send({
                    status: false,
                    message: "Bạn chưa đăng nhập tài khoản vào extention"
                })
            }
            
        })
        app.route('/writelog').post(async (req,res) => {
            // if(req.session.username){
            try {
                const { fileName, jsonData } = req.body;
                const writeable = await writeJsonFile( `app/logs/${fileName}`, jsonData);
                res.send({
                    status: writeable,
                    message: `Ghi file ${ fileName } hoàn thành`,
                })
            } catch (error) {
                res.send({
                    status: false,
                    message: `Ghi file ${ fileName } thất bại`,
                    data: error
                })
            }
            // } else {
            //     res.send({
            //         status: false,
            //         message: "Bạn chưa đăng nhập tài khoản vào extention"
            //     })
            // }
        })
        app.route('/readlog').post(async (req,res) => {
            // if(req.session.username){
                let { fileName } = req.body;
                try {
                    const data = await readJsonFile(`app/logs/${fileName}`)
                    res.send({
                        status: true,
                        message: `Đọc file ${ fileName } thành công`,
                        data: data
                    })
                } catch (error) {
                    res.send({
                        status: false,
                        message: `Đọc file ${ fileName } thất bại`,
                        data: error
                    })
                }
                
            // } else {
            //     res.send({
            //         status: false,
            //         message: "Bạn chưa đăng nhập tài khoản vào extention"
            //     })
            // }
        })
        app.route('/updatelog').post(async (req,res) => {
            // if(req.session.username){
            try {
                const { fileName, jsonData } = req.body;
                const writeable = await updateJsonFile( `app/logs/${fileName}`, jsonData);
                res.send({
                    status: writeable,
                    message: `Cập nhật file ${ fileName }  ${writeable ? 'hoàn thành': 'thất bại'}`,
                })
            } catch (error) {
                res.send({
                    status: false,
                    message: `Cập nhật file ${ fileName } thất bại`,
                    data: error
                })
            }
            // } else {
            //     res.send({
            //         status: false,
            //         message: "Bạn chưa đăng nhập tài khoản vào extention"
            //     })
            // }
        })
        
        app.route('/readalllog').post(async (req,res) => {
            // if(req.session.username){
             
                try {
                   
                    const filesList =  await fspm.readdir(`app/logs`);
                    const jsonList = filesList.filter(function(e){
                        return path.extname(e).toLowerCase() === '.json'
                      });
                    
                    res.send({
                        status: true,
                        message: `Đọc logs thành công`,
                        data: jsonList
                    })
                    
               
                    
                } catch (error) {
                    console.log(error)
                    res.send({
                        status: false,
                        message: `Đọc logs thất bại`,
                        data: error
                    })
                }
                
          
        })
        //handle invalid router
        app.get('/*', (req, res) => {
            
            res.redirect('/')
        })
    } catch (error) {
        console.log('Router Cacher:', error);
    }
};


