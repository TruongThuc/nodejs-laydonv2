const  puppeteer  = require('puppeteer');
const {
    js1,
    js3,
    js4,
    js5
} = require('./exec')

const mouseSlide = async (page) => {
    let bl = false
    while (!bl) {
        try {
            await page.hover('#nc_1_n1z');
            await page.mouse.down();
            await page.mouse.move(2000, 0, {
                'delay': 1000 + Math.floor(Math.random() * 1000)
            });
            await page.mouse.up();
  
            slider_again = await page.$eval('.nc-lang-cnt', node => node.textContent);
            console.log('slider_again', slider_again);
            if (slider_again != '验证通过') {
                bl = false;
                await page.waitFor(1000 + Math.floor(Math.random() * 1000));
                break;
            };
            await page.screenshot({
                'path': path.join(__dirname, 'screenshots', 'result.png'),
            });
            console.log('验证通过');
            return 1;
        } catch (e) {
            console.log('error :slide login False', e);
            bl = false;
            break;
        }
    }
}

module.exports = async (browser, loginURL, username, password) => {
    const page = await browser.newPage();
    page.setViewport({ width: 1376, height: 1376 });
    await page.goto(loginURL, {
      waitUntil: 'networkidle2'
    });
    //overriding page setting 
    await page.evaluate(js1);
    await page.waitFor(1 + Math.floor(Math.random() * 1000));
    await page.evaluate(js3);
    await page.waitFor(1 + Math.floor(Math.random() * 1000));
    await page.evaluate(js4);
    await page.waitFor(1 + Math.floor(Math.random() * 1000));
    await page.evaluate(js5);
    await page.waitFor(1 + Math.floor(Math.random() * 1000));
    
    try {
        await page.waitForSelector('#J_Quick2Static', {visible: true, timeout: 500});
        console.log('current quick login - go to static login');
        await page.click('#J_Quick2Static');
    } catch (error) {
        if (error instanceof puppeteer.errors.TimeoutError) {
           console.log('current static login');
        }
    }
    
    await page.waitFor(Math.floor(Math.random() * 500) * Math.floor(Math.random() * 10));
    const opts = { 
        delay: 2 + Math.floor(Math.random() * 2), 
    }

    let usernameClass = ''; //#TPL_username_1 / #fm-login-id
    let passwordClass = ''; //#TPL_password_1 / #fm-login-password
    let submitBtnClass = ''; //#J_Form #J_SubmitStatic / #login-form .fm-submit
    let captchaClass = '';
    try {
        await page.waitForSelector('#J_Form', {visible: true, timeout: 3000});
        usernameClass = '#TPL_username_1';
        passwordClass = '#TPL_password_1';
        submitBtnClass = '#J_Form #J_SubmitStatic';
        captchaClass = '#nocaptcha';
    } catch (error) {
        usernameClass = '#fm-login-id';
        passwordClass = '#fm-login-password';
        submitBtnClass = '#login-form .fm-submit';
        captchaClass = '#nocaptcha-password';
    }
    await page.tap(usernameClass); //TPL_username_1 / fm-login-id'
    await page.type(usernameClass, username, opts);
    await page.waitFor(100);
  
    await page.tap(passwordClass); //TPL_password_1 / fm-login-password
    await page.type(passwordClass, password, opts);
  
    // await page.screenshot({
    //   'path': path.join(__dirname, 'screenshots', 'login.png'),
    // });
  
    const slider = await page.$eval(captchaClass, node => node.style);
    if (slider && Object.keys(slider).length) {
    //   await page.screenshot({
    //     'path': path.join(__dirname, 'screenshots', 'login-slide.png'),
    //   });
        try {
            await page.waitForSelector(captchaClass, {visible: true, timeout: 3000});
            await mouseSlide(page);
        } catch (error) {
            
        }
       
    }
  
    await page.waitFor(Math.floor(Math.random() * 1000));
    
    // #J_Form #J_SubmitStatic / #login-form .fm-submit
    let loginBtn = await page.$(submitBtnClass); 
    
    await loginBtn.click({
      delay: 20,
    })
  
    await page.waitFor(20);
    await page.waitForSelector(".site-nav-login-info-nick",{visible: true, timeout: 120000});
    try {
        const error = await page.$eval('.error', node => node.textContent);
        if (error) {
            console.log("login error", error);
            // process.exit(1);
            // return {status: false, message: "Cấu hình lại tài khoản của bạn, và đảm bảo rằng tài khoản của bạn an toàn....."}
            return {status: false, message: "Đăng nhập thất bại, vui lòng kiểm tra lại thông tin đăng nhập"};
        }
    } catch (e) {
        return page;
    }
    return false;
}


