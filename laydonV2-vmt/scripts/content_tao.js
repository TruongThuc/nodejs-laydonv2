const API_HOST = 'https://extension.monamedia.net//webservice1.asmx';
const domain = 'https://vominhthien.com';

const rootIdSelector = '';
const BRAND_FULL = 'Mona media';
const BRAND_SHORT = 'Mona';
const EXT_TITLE = "Hệ thống extension tự động lấy link của Mona Media";
const isRequired = (name = "param") => { throw new Error(name+' is required'); };
let processAuto = false;
const sequenceControler = async (data, wrap) => {
    try {
        const stp_1 = await checkAvailableOrders(data.list, wrap);
        console.log("stp_1:", stp_1);
        if(!stp_1) throw "sequence checkAvailableOrders fail!!!";
        if(data.isAuto){
            $(wrap).find('.btnextaction.btntoggleauto').html("Tắt chạy tự động");
            if(data.userName){

                const stp_2 = await trackingOrders(data.list, wrap);
                console.log("stp_2:", stp_2);
                if(!stp_2) throw "sequence trackingOrders fail!!!";

                //final => begin new sequence
                setTimeout(function(){ location.reload(); }, 60000 * 1);
            } else {
                console.log('sequence fail, Bạn chưa login tài khoản trong extentions');
                $("#extPopLogin").find("#fstatus").html("<span style='color:red'>Chạy tự động thất bạn,Bạn chưa login tài khoản trong extention</span>");
                $(wrap).find('.btnextaction.btntoggleauto').html("Mở chạy tự động");
                // Swal.fire({
                //     title: '',
                //     type: 'error',
                //     html: `<p>Chạy tự động thất bạn,Bạn chưa login tài khoản trong extentions</p>`
                // });
            }
        } else {
            $(wrap).find('.btnextaction.btntoggleauto').html("Mở chạy tự động");
        }
    } catch (error) {
        console.log(error);
        if(error === "sequence trackingOrders fail!!!"){
            $(wrap).find('.btnextaction.btntoggleauto').html("Mở chạy tự động");
        }

    }
}

const sendMessagePromise = reqItem => {
    return new Promise((resolve, reject) => {
        chrome.runtime.sendMessage(reqItem, response => {
            if(response.handlerReceive) {
                resolve(response);
            } else {
                reject('Something wrong');
            }
        });
    });
}
const sendAjaxPromise = ({url = isRequired("url key") , type = isRequired("type key") , data = isRequired("data key")} = isRequired()) => new Promise((resolve, reject) => { 
        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function(result) {
                resolve(result);
            },
            error: function(error) {
                reject(error);
            }
        });
});
const CallbackCodeHandler = res =>{
    // console.log(res);
    const codeHandlerConfig = {
        "101": () => ({status: false, message: res.Message, actionCode: "LOGIN_REDIRECT_WEBAPP"}),
        "102": () => ({status: true,  ...res}),
        "103": () => ({status: false, message: res.Message, actionCode: "SIMPLE_ERR", ...res}),
        "default": () => ({status: false, ...res})
    }
    return !codeHandlerConfig[res.Code] ? codeHandlerConfig["default"] : codeHandlerConfig[res.Code];
    
}
const updateSingleOrder = async request => {

    try {
        const { data: response} = await sendMessagePromise({CallAction: "mid_fetchdetail", data: request});
      
        console.log("Detail data:", response);
        //
        if(response.hasOwnProperty("error")) throw response;
        if(response.status){
            let isCancel = {};
            if(!response.data) response.data = {};
            if(response.data.type == 1){
                //TAOBAO
                let {deliveryInfo} = response.data;
                if(!deliveryInfo.showLogistics){
                    if(!deliveryInfo.logisticsNum){
                        // đơn hàng trong TAOBAO đã huỷ
                        isCancel = {isCancel: true}
                    }
                }

            } else if(response.data.type == 2){
                //tmall

            } else if(response.data.type == 3){
                //1688 
                isCancel =  {isCancel: response.data.isCancel};
            }
            console.log('call to webapp...');
            const pushHostRq = {data:response.data, odCode: request.oId, type: response.data.type};
            let {data : updateRes} = await sendMessagePromise({CallAction: "updateOrder", data: pushHostRq});
            console.log('call to webapp done!...');
            return  CallbackCodeHandler({...updateRes, ...isCancel});

        } else {
            throw {error: response.message};
        }
    } catch (error) {
        console.log("updateSingleOrder:" ,error);
        return error;
    }
}
const trackingOrders = async (list, wrap) => {
 
    for (const element of list) {
        if(element.isLogistic && $(element.html).hasClass('getable')){
            const orderItem = {detailurl: location.protocol + element.link, html: element.html};
            switch (true) {
                case orderItem.detailurl.indexOf("taobao.com") > 0:
                    orderItem.type = 1;
                    orderItem.detailurl = orderItem.detailurl.replace("buyertrade.taobao.com/trade/detail/trade_item_detail.htm", "trade.taobao.com/trade/detail/trade_order_detail.htm");
                    break;
                case orderItem.detailurl.indexOf("tmall.com") > 0:
                    orderItem.type = 2;
                    break;
                case orderItem.detailurl.indexOf("1688.com") > 0:
                    orderItem.detailurl = element.link;
                    orderItem.type = 3;
                    break;
                default:
                    break;
            }
            orderItem.oId =  element.codeOrder;
            $(element.html).css({'background-color': "#0000ff38"});
            try {

                const fallbackResult = await updateSingleOrder(orderItem);
                
                if(fallbackResult.hasOwnProperty("error")){ throw fallbackResult}
                const result = fallbackResult();
                
                if(result.status === false ){
                    if(result.actionCode === "LOGIN_REDIRECT_WEBAPP"){
                        $(element.html).attr({'style': ""});

                        Swal.fire({
                            title: '',
                            type: 'error',
                            html: `<p>Bạn phải login vào hệ thống ${ BRAND_FULL } để sử dụng chức năng này</p>
                            <p>Vui lòng vấm vào nút bên trái, phía dưới để login</p>
                            <p>Sau khi login ở tab mới bạn chỉ cần tắt thông báo này và bấm lại vào nút cập nhật</p>`,
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText: 'Bấm vào đây để login',
                            cancelButtonText: 'Quay lại',
                            showLoaderOnConfirm: true,
                            preConfirm: (value) => {
                                return  window.open(result.message) ;
                            }
                        });
                        return false;
                        break; 

                    } else {
                        $(element.html).css({'background-color': "#ffcaca"});
                    }
                } else {
                    $(element.html).css({'background-color': "#00ff66"});
                }
                
            } catch (error) {
                console.log(error);
                if(error.error === "Failed to fetch") error.error = "Chưa mở ứng dụng lấy dữ liệu đơn hàng";
                if(error.error.name === "TimeoutError") return true;
                Swal.fire({
                    title: '',
                    type: 'error',
                    html: `<p>${error.error}</p>`
                });
                $(element.html).css({'background-color': "#ffcaca"});
                return false;
                break;
            }
            
        } else {

        }
    } // end loop
    return true;
    console.log('tracking Orders finish!');
}


//listen chrome event

// chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
//     console.log(sender.tab ?
//                 "from a content script:" + sender.tab.url :
//                 "from the extension");
//     if (request.action == "UpdateListItem"){
//         console.log(request);
//         sendResponse({UpdateListItem: "success"});
        
//         const orderId = request.orderId;
//         let $targetItem = $('.extnh-wrap').find('[data-order-id='+orderId+']');
//         if($targetItem){
//             $targetItem.find(".clstatus").prepend('Đã Bấm');
//         }
//     }
//     // return true; // return when this message Response is a async message
// });

if(location.host == "buyertrade.taobao.com"){

    let wrap = document.createElement("DIV");
   
    if($('#J_bought_main').length > 0){
        $(wrap).prependTo($('#J_bought_main'));
        injectTBL_HTML(wrap);
    }
   
    // mutation
    if($('#J_Col_Main').length > 0){
        var mutationObserver = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if(mutation.type === "childList"){
                
                    if(mutation.target.id === "tp-bought-root"){
                        
                        injectTBL_HTML(wrap);

                    }
                }
                
            });
        });
    
        // Starts listening for changes in the root HTML element of the page.
   
        mutationObserver.observe(document.getElementById('J_Col_Main'), {
            attributes: true,
            characterData: true,
            childList: true,
            subtree: true,
            attributeOldValue: true,
            characterDataOldValue: true
        });
    }
    if($('#nc-verify-form').length > 0){
        //
        alert("page verify");
        
        console.log("page verify");
        chrome.runtime.sendMessage({CallAction: "GET_EXTENTION_STATUS"}, function(response) {
            const extStatus = response.data;
            console.log(extStatus);
        });
        
    }
    
    
} else if(location.host == "trade.taobao.com"){
    
    let wrap = document.createElement('DIV');
    let updateBtn = $('<a class="btnextaction" id="mn_update_btn">Update</a>');
    $('.crumbs-mod__container___2OwwW').append(wrap);
    injectExtWrap(wrap);
    $(wrap).addClass('nhext-detail-wrap');
    updateBtn.appendTo($(wrap)); 
    
    updateBtn.on('click', function(){
        
        var $dataScript = $(document).find('#page').next();
        var pageJson = eval($dataScript[0].innerHTML.split('data =')[1]);
        const arrayFlow = () => {
            try {
                return Array.from($('.logistics-info-mod__list___2KLt8').find('li')).map(item =>{
                    return item.textContent;
                });
            } catch (error) {
                return [];
            }
        }
        pageJson.logicticsFlow = arrayFlow();
        console.log(pageJson);
        updateOrder(this, pageJson.mainOrder.id, pageJson, 1);
        
    });
    // updateBtn.trigger('click');
        
} else if(location.host == "trade.tmall.com"){
    let wrap = document.createElement('DIV');
    let updateBtn = $('<a class="btnextaction" id="mn_update_btn">Update</a>');
    $('#page').prepend(wrap);
    injectExtWrap(wrap);
    $(wrap).addClass('nhext-detail-wrap');
    updateBtn.appendTo(wrap);
    
    const $dataScript = $(document).find('#content').find('script').map((index,item) => {
        return item.innerHTML.trim();
    });
    console.log($dataScript);
    let pageJson;
    $dataScript.each(function(){
        let html = this;
        console.log(html);
        if(html.indexOf("detailData") > 0){
            
            pageJson = JSON.parse(html.split("detailData =")[1]);

            console.log(html.split("detailData =")[1]);
            
            pageJson.ad = null;
            pageJson.top = null;
            pageJson.orders.id = getUrlParameter("bizOrderId");
            console.log(pageJson);
            
            
            
            
            updateOrder(this, pageJson.orders.id, pageJson, 2);
        }
    });

    updateBtn.on('click', function(){
        pageJson.logicticsFlow = ( () => {
            try {
                const ulFlow = document.querySelector('.trade-detail-logistic');
                return Array.from(ulFlow.querySelectorAll('.logistic-detail')).map(item => {
                    return item.textContent
                });
            } catch (error) {
                console.log(error);
                return [];
            }
        } )();
        console.log(pageJson);
        updateOrder(this, pageJson.orders.id, pageJson, 2);
        
    });
    // updateBtn.trigger('click');
} else if(location.host == "trade.1688.com"){
    if(location.pathname == "/order/new_step_order_detail.htm"){
        //detail page
        let updateBtn = $('<a class="btnextaction" id="mn_update_btn">Update</a>');
        let extwrap = $('<div class="nhext-detail-wrap mod-margin-top" />');
        let extNote = $('<div class="extNote" style="margin: 10px 0"></div>');
        let packetLoadMess = $('<span class="pkloaded" style=""></span>');
        injectExtWrap(extwrap[0]);
        extwrap.append(updateBtn);
        extwrap.append(extNote);
        $('#content .screen').prepend(extwrap);
        extwrap.addClass('nhext-detail-wrap');
        $('#logisticsTabTitle').trigger('click');
        
        console.log('load logistics info');
        var sokien = $('.logistics-item').length;
        var packetLoad = sokien;
        extNote.html('Có '+ sokien +' kiện hàng trong đơn hàng này. ');
        extNote.append(packetLoadMess);
        let btm_extWrap = extwrap.clone(true,true);
        $('#content .screen').append(btm_extWrap);
        if(sokien > 2){
            
            extNote.append("<br>Vui lòng kéo xuống cuối trang để tiến hàng cập nhật tình trạng các kiện hàng.");
        }
        
        mutationWatching( $('.submod-logistics-info')[0], function(mutations){
            
            mutations.forEach(function(mutation) {
                if(mutation.type == "attributes" && mutation.attributeName == "class" 
                && $(mutation.target).hasClass('SIGN') && mutation.oldValue == "logistics-item"){
                    packetLoad = packetLoad - 1;
                    packetLoadMess.html('Đã quét: ' + (sokien - packetLoad) + '/' + sokien + ' kiện');
                    btm_extWrap.find('.pkloaded').html('Đã quét: ' + (sokien - packetLoad) + '/' + sokien + ' kiện');
                }
                if(mutation.target.className == "logistics-item"){
                    // console.log('logistics loaded', mutation);
                } 
                if(mutation.type == "characterData"){
                    console.log('translate data');
                }
            });

            // doTranslateRequest({sourceText: "物流公司"});
            
        });
       
        
        
        updateBtn.on('click', function(){
            let orderID = getUrlParameter("orderId");
            console.log(orderID);
            
            let orderObject = {};
            let orderDetail = {};
            let orderLogistics = [];
            let orderContract = {};

            let $orderDetail = $('.submod.submod-order-info');
            let $orderLogistics = $('.submod-logistics-info');
            let $orderContract= $('.submod-trade-mode-info');
            let logicticArray = [];
            let $buyer = $orderDetail.find('.unit-buyer-info');
            let $seller = $orderDetail.find('.unit-seller-info');
            
            orderDetail.buyer = {
                id : $buyer.find('ul > li').eq(0).text().split('订单号：')[1].trim(),
                idAlipay : $buyer.find('ul > li').eq(1).text().split('支付宝交易号：')[1].trim(),
                codebill : $buyer.find('ul > li').eq(2).text().split('收货人：')[1].trim(),
                address : $buyer.find('ul > li').eq(3).text().split('收货地址：')[1].trim(),
                phone : $buyer.find('ul > li').eq(4).text().split('手机：')[1].trim(),
                phone2 : $buyer.find('ul > li').eq(5).text().split('电话：')[1].trim()
            }
            orderDetail.seller = {
                supplier :  [$seller.find('ul > li').eq(0).find('a[data-tracelog="viewUserCompany"]').text().trim(), 
                $seller.find('ul > li').eq(0).find('a[data-tracelog="viewUserCompany"]').attr('href')],
                user : [$seller.find('ul > li').eq(1).find('a[data-tracelog="viewUserProfile"]').text().trim(), 
                $seller.find('ul > li').eq(1).find('a[data-tracelog="viewUserProfile"]').attr('href')],
                userAlipay: $seller.find('ul > li').eq(2).text().split('支付宝账户：')[1].trim(),
                phone : $seller.find('ul > li').eq(3).text().split('手机：')[1].trim(),
                phone2 : $seller.find('ul > li').eq(4).text().split('电话：')[1].trim()
            }
            orderDetail.subtotal = $orderDetail.find('.unit-order-list .total-cell').text().trim();
            orderDetail.shippingPrice = $orderDetail.find('.unit-order-total .label-line').eq(0).text().trim().split('运费：')[1].trim();
            orderDetail.total = $orderDetail.find('.unit-order-total .total-price').text().trim();
            orderDetail.productList = [];
            $orderDetail.find('.unit-order-list tbody .item').each(function(){
                
         
                orderDetail.productList.push({
                    imgSRC: $(this).find('.cell-thumbnail img').attr('src'),
                    pdName: $(this).find('.cell-thumbnail .offer-title').text().trim(),
                    pdPrice: $(this).find('td').eq(1).text().trim(),
                    quantity: $(this).find('td').eq(2).text().trim()
                });
                
            });
            console.log(orderDetail.productList);
            // push logicticArray
            $('.logistics-item').each(function(){
                let logicticArray = [];
                let packetProductArray = [];
                let singleLogistic = {}
                singleLogistic.id =  $(this).find('.item-content .item-list').find('.info-item').eq(0).find('.info-item-val').text().trim();
                singleLogistic.company =  $(this).find('.item-content .item-list').find('.info-item').eq(1).find('.info-item-val').text().trim();
                singleLogistic.mailNumber =  $(this).find('.item-content .item-list').find('.info-item').eq(2).find('.info-item-val').text().trim();
                singleLogistic.shipTime =  $(this).find('.item-content .item-list').find('.info-item').eq(3).find('.info-item-val').text().trim();
                if($(this).hasClass('SIGN')){
                    $(this).find('.logistics-flow .logistics-flow-item').each(function(){
                        let flowItem = this;
                        if($(flowItem).hasClass('first-date')){
                            let dateFlow = [];
                            dateFlow.push({
                                time : $(flowItem).find('.time').text(),
                                area : $(flowItem).find('.area-name').text(),
                                remark: $(flowItem).find('.remark').text()
                            }); 
                            logicticArray.push({date : $(flowItem).find('.date').text() + " " + $(flowItem).find('.time').text(),
                            detail : dateFlow});
                        } else {
                            logicticArray[logicticArray.length - 1].detail.push({
                                time : $(flowItem).find('.date').text() + $(flowItem).find('.time').text(),
                                area : $(flowItem).find('.area-name').text(),
                                remark: $(flowItem).find('.remark').text()
                            });
                        }
                    });
                } else {
                    logicticArray = [];
                }
                    
                let jo_pkitem = $(this).find('.unit-logistics-prodlist .logistics-prodlist').find('.prod-item');
                jo_pkitem.each(function(){
                    let sgimg = $(this).find('.a-img img');
                    packetProductArray.push({
                        img : sgimg.attr('src'), title: sgimg.attr('alt'),

                    });
                });
                singleLogistic.products =  packetProductArray;
                singleLogistic.flow =  logicticArray;
                orderLogistics.push(singleLogistic);
            });
                
            
            
      

            //mapping
            orderObject.detail = orderDetail;
            orderObject.logistics = orderLogistics;
            orderObject.contract = orderContract;
            
            console.log(orderObject);
            updateOrder(this, orderID, orderObject, 3);
        });
        btm_extWrap.find('.btnextaction').on('click', function(){
            updateBtn.trigger('click');
        });
        // updateBtn.trigger('click');
    } else {
        //list page
        let wrap = document.createElement("DIV");
        $(wrap).css({"margin": "15px 0"});
        $(wrap).prependTo($('#app-content'));

        inject1688L_HTML(wrap);

        // mutation
        var mutationObserver = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if(mutation.type === "childList"){
                
                    if(mutation.target.id === "new-content"){
                        
                        inject1688L_HTML(wrap);

                    }
                }
                
            });
        });

        // Starts listening for changes in the root HTML element of the page.
        mutationObserver.observe(document.getElementById('orderList'), {
            attributes: true,
            characterData: true,
            childList: true,
            subtree: true,
            attributeOldValue: true,
            characterDataOldValue: true
        });
    }
    
    
} else if(location.host == "work.1688.com"){
    if(location.pathname == "/home/buyer.htm"){
        mutationWatching( document , function(mutations){
            mutations.forEach(function(mutation){
                if(mutation.target.nodeName == "DIV" && mutation.target.className == "iframe-container"){
                    if(mutation.addedNodes.length > 0){
                        var ifSrc = mutation.addedNodes[0].getAttribute('src');
                        window.location.href = ifSrc;
                    }
                }
            });
        });
    }
       
} else {

}
function mutationWatching(HTMElement, callback){
    var mutationObserver = new MutationObserver(function(mutations, ob) {
        callback(mutations,ob);
    });

    // Starts listening for changes in the root HTML element of the page.
    mutationObserver.observe(HTMElement, {
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
    });
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.href.split(location.host)[1]);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
function updateOrder(target, odCode, data, type = ''){

    const $processSS = $(document.createElement('P'));
    $(target).parent().append($processSS);
    if(parseInt(type) > 0 && parseInt(type) > 3 ){
        alert('Không đúng loại trang !! ')
        return false;
    }
    if($(target).hasClass('loadingbtn')){
        alert('Bạn ơi bấm từ từ thôi !! Server chịu không nổi bạn click');
        return false;
    }
    let value = JSON.stringify(data);
    $(target).addClass('loadingbtn').html('Đang xử lý....');
    $.ajax({
        url: API_HOST+'/receivesmallpackage',
        method: 'POST',
        data: {
            MainOrderCode: odCode,
            Value: encodeURI(value),
            Type: type
        }
    }).done(function( msg ){

        console.log("result: " , msg);
        switch(msg.Code){
            case "101":
                Swal.fire({
                    title: '',
                    type: 'error',
                    html: `<p>Bạn phải login vào hệ thống ${ BRAND_FULL } để sử dụng chức năng này</p>
                    <p>Vui lòng vấm vào nút bên trái, phía dưới để login</p>
                    <p>Sau khi login ở tab mới bạn chỉ cần tắt thông báo này và bấm lại vào nút cập nhật</p>`,
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: 'Bấm vào đây để login',
                    cancelButtonText: 'Quay lại',
                    showLoaderOnConfirm: true,
                    preConfirm: (value) => {
                        return  window.open(msg.Message) ;
                    }
                  });
                break;
            case "102":
                chrome.runtime.sendMessage({UpdateDetail: "Done", "orderId": odCode}, function(response) {
                    console.log(response);
                });
                // alert('Cập nhật thành công');
                $processSS.html('Cập nhật thành công');
                break;
            case "103":
                Swal.fire({
                    title: '',
                    type: 'error',
                    html: `<p>${ msg.Message }</p>`
                });
                break;
            default:
              
                Swal.fire({
                    title: '',
                    type: 'error',
                    html: `<p>Lỗi !!, dữ liệu trả về không đúng, vui lòng liên hệ nhà cung cấp:</p>
                    <p>${msg.Message}</p>`
                });
                break;
        }
    }).fail(function( jqXHR, textStatus ) {
        console.log("Request failed: " , textStatus);
        console.log(jqXHR);
        alert("Request failed: " , textStatus);
    }).always(function(){
        $(target).removeClass('loadingbtn').html('Update');
        $processSS.fadeOut(function(){$processSS.remove()});
    });
}
const getOrderList = async () => {
    let statusF = false;
    const res = await sendMessagePromise({ 
        CallAction: "getOrderListFull", 
        data: {} 
    });
    console.log("getOrderListFull ", res);
    const { data } = res;
    switch(data.Code){
        case "101":
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>Bạn phải login vào hệ thống ${ BRAND_FULL } để sử dụng chức năng này</p>
                <p>Vui lòng vấm vào nút bên trái, phía dưới để login</p>
                <p>Sau khi login ở tab mới bạn chỉ cần tắt thông báo này và bấm lại vào nút cập nhật</p>`,
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: 'Bấm vào đây để login',
                cancelButtonText: 'Quay lại',
                showLoaderOnConfirm: true,
                preConfirm: (value) => {
                    return  window.open(data.Message) ;
                }
            });
            break;
        case "102":
            // thành công
              return data.data

            break;
        case "103":
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>${ data.Message }</p>`
            });
         
            break;
        default:
          
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>Lỗi !!, dữ liệu trả về không đúng, vui lòng liên hệ nhà cung cấp:</p>
                <p>${JSON.stringify(data)}</p>`
            });
            break;
    }
    return false;
}
const getCurrentTimeString = () =>{
    const d = new Date();
    return {
        date: `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()}`,
        time: `${d.getHours()}:${d.getMinutes()}`,
        formated: `${d.getDate()}-${d.getMonth() + 1}-${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`
    }
    
}
const appendLogFile = async (name, data) => {
    let req_Data = {
        fileName: `${name}.json`, 
        jsonData: data
    }
    try {
        const res = await sendMessagePromise({
            CallAction: "mid_updatelog",
            data: req_Data 
        });
        const resData = res.data;
        console.log(resData);
        if(resData.status){
            //success
        } else {
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>${resData.message}</p>`
            });
        }
    } catch (error) {
        console.log(error);
    }
}
const checkIsAuto = async indicatorBtn =>{
    let buttonToggle = $(indicatorBtn);

    const {data : resSetting} = await sendMessagePromise({CallAction: "GET_EXTENTION_STATUS"});
    if(buttonToggle.length > 0){
        resSetting.isAuto ? indicatorBtn.html('Tắt chạy tự động') : indicatorBtn.html('Mở chạy tự động');
    }
    
    return resSetting.isAuto;

}
const doAutoPassData = async (buttonToggle, logsDiv, isAuto) => {
    
    const $logsDiv = $(logsDiv);
    
    if(!isAuto) {
        await sendMessagePromise({CallAction: "TURNOFF_AUTO"});
        buttonToggle.addClass('loading ').css({'pointer-events': 'none'});
        $logsDiv.closest('.extnh-wrap').find('.ext-btnwrap').css({'pointer-events': 'none'});
        buttonToggle.html('Đang tiến hành tắt chạy tự động');
        // $logsDiv.html('');
        // return;
    } else {
        buttonToggle.html('Tắt chạy tự động');
        $logsDiv.append('<p>Bắt đầu quét đơn hàng...</p>');
        const listorder = await getOrderList(buttonToggle);
        // const listorder = [{MainOrderCode: '844567585087226418', Site: '1688'}]
        if(!listorder.length){
            await sendMessagePromise({CallAction: "TURNOFF_AUTO"});
            buttonToggle.html('Mở chạy tự động');
            $logsDiv.html('');
            return ;
        }

        console.log("listorder.length", listorder.length );
        
        $logsDiv.append(`<p>Tìm thấy ${ listorder.length } đơn trong hệ thống...</p>`);
        const getMakeupLinkTemplate = {
            'TMALL' : 'https://trade.tmall.com/detail/orderDetail.htm?bizOrderId=',
            'TAOBAO' : 'https://trade.taobao.com/trade/detail/trade_order_detail.htm?biz_order_id=',
            '1688' : 'https://trade.1688.com/order/new_step_order_detail.htm?orderId='
        }
        $logsDiv.append(`<p>Bắt đầu lấy dữ liệu từng đơn hàng trong Taobao về hệ thống..</p>`);
        for (const [i ,{MainOrderCode: orderID , Site: site}] of listorder.entries()) {
            
            // check isAuto before continue loop
            let isAuto = await checkIsAuto();
            if(!isAuto) break; 
            // begin loop next item
            console.log(i + 1, orderID, site);

            let urlMakeup = getMakeupLinkTemplate[site];
            
            let orderIdArray = orderID.split("-");
            for (const value of orderIdArray){
                let logString = ``;
                let pageIndicator = $('<div></div>')
                $logsDiv.prepend(pageIndicator);
                let urlDetail = '';
                const process_status = {
                    access: false,
                    write: false,
                    isUpdate: false
                }
                const executedTime =  getCurrentTimeString();
                urlMakeup = urlMakeup.trim() + value.trim();
                let linkOd = `<a href="${ urlMakeup }" target="_blank">${site} ${value.trim()}</a>`;
                pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } <i class="loadingbtn"></i></p>`);
                try {
                    let typeIndex = ['TAOBAO', 'TMALL', '1688'].indexOf(site);
                    pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[Checking]</span> <i class="loadingbtn"></i></p>`);
                    const fallbackResult = await updateSingleOrder({
                        detailurl: urlMakeup, 
                        oId: value, 
                        type: typeIndex + 1 
                    });
                    if(fallbackResult.error){
                    
                        process_status.access = false;
                        pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[false]</span> - <span>Write[Checking] <i class="loadingbtn"></i></p>`);
                        if(fallbackResult.error.name === "TimeoutError"){
                            
                            console.log(`Thời gian chờ quá lâu(>30000ms). \nLấy liệu nội dung đơn ${ value.trim() } thất bại.`);
                        } else {
                            fallbackResult.error == "Failed to fetch" &&  (fallbackResult.error = "Chưa mở ứng dụng lấy dữ liệu đơn hàng");
                            if(fallbackResult.error ==  "URL invalid"){
                                pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[false]</span> - <span>Write[Checking] <i class="loadingbtn"></i></p>`);
                            } else if(fallbackResult.error ==  "datanull"){
                                pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[false]</span> - <span>Write[Checking] <i class="loadingbtn"></i></p>`);
                            } else {
                                pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[false]</span> - <span>Write[Checking] <i class="loadingbtn"></i></p>`);
                               // Swal.fire({
                               //     title: '',
                               //     type: 'error',
                               //     html: `<p>${fallbackResult.error}</p>`
                               // });
                               // throw "turnoff";
                            }
                            
                        }
    
                    } else {
                        
                        process_status.access = true;
                        pageIndicator.html(`<p>${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span>Access[true]</span> - <span>Write[Checking]</span> <i class="loadingbtn"></i></p>`);
                        const result = fallbackResult();
                        console.log(result);
                        urlDetail  =  result.Link;
                        if(!result.status || result.status === 500){
                            process_status.write = false;
                            if(result.actionCode === "SIMPLE_ERR"){
                                result.isCancel && (process_status.writeMess = 'Đơn hàng đã huỷ');
                                //server error
                                console.log(`Code: ${result.Code}. \nCập nhật đơn ${  value.trim() } vào hệ thống thất bại.`);
                            
                            } else {
                                console.log('write to webapp fail:', result);
                                // Swal.fire({
                                //     title: '',
                                //     type: 'error',
                                //     html: `<p>${fallbackResult.error} <br> ${ urlMakeup }</p>`
                                // });
                            }
                            
                        
                        } else {
                            process_status.write = true;
                            if(result.Message.length > 0){
                                process_status.isUpdate = true;
                                process_status.updateMessage = result.Message;
                            }
                            console.log(`Cập nhật đơn ${  value.trim() } thành công`);
                        
                        }
                    }
                    //23-03-2020 - 16:30: 123123123123 - Access [Pass] - Write [Pass] - Cập nhật mới ở hệ thống [Lỗi - Không có - Thành Công]
                    let data = {};
                    let isHl = !(process_status.access && process_status.write);
                    if(!process_status.updateMessage) process_status.updateMessage = "Không có";
                    if(!process_status.writeMess) {process_status.writeMess = ''} else {process_status.writeMess = '-' + process_status.writeMess};
                    
                    logString = `<p class="${ !!isHl ? 'hl' : '' } ">${ i + 1 }.${executedTime.formated}: Lấy đơn ${ linkOd } - <span class="${ !!process_status.access ? 'pass' : 'fail' }">Access[${process_status.access}]</span> - <span class="${ !!process_status.write ? 'pass' : 'fail' }">Write[${process_status.write}${process_status.writeMess}]</span> - <span class="${ !!process_status.isUpdate ? 'isUpdate' : '' }">Cập nhật hệ thống[${process_status.updateMessage}]</span> ${ urlDetail ? `<a href="${ urlDetail }"  target="_blank">Chi tiết</a>` : ``}</p>`;
                    
                    // ${executedTime.formated}: ${ logString }
                    data[value.trim()] = {
                        url: urlMakeup,
                        ishl: isHl,
                        logs: logString
                    };
                    
                    // console.log(isHl, process_status);
                    pageIndicator.html(logString)
                

                    await appendLogFile(executedTime.date, {
                        total: listorder.length,
                        ...data
                    });
                    console.log('write log', logString);
                } catch (error) {
                    console.log(error);
                    await sendMessagePromise({CallAction: "TURNOFF_AUTO"});
                    $logsDiv.html('');
                    break;
                }
                
            }
            
            let isAuto2 = await checkIsAuto();
            if(!isAuto2) {
                buttonToggle.removeClass('loading').attr('style', '');
                buttonToggle.html('Mở chạy tự động');
                $logsDiv.closest('.extnh-wrap').find('.ext-btnwrap').attr('style', '');
                $logsDiv.html('');
                break;
            }
            // console.log("stop for debug, only run 1 item")
            // break; // stop for loop
        }
        
    }
}
const readJsonFileLog = async fileName =>{
    try {
        const res = await sendMessagePromise({
            CallAction: "mid_readlog", 
            data: fileName
        });
        console.log(res);
        const resData = res.data;
        if(resData.status){
            //success
            const jsonData =  JSON.parse(resData.data);
            // console.log(jsonData);
            return jsonData;
        } else {
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>${resData.message}</p>`
            });
        }
    } catch (error) {
        console.log(error);
    }
}

function injectTBL_HTML(wrap){
    
    injectExtWrap(wrap);
    let pageData = dingdan_array();
    // console.log(pageData);
    // let list = createDataHTML(pageData);
    // $(list).appendTo(wrap);
    const $btn = $('<a class="btnextaction" href="javascript:;" style="margin: 10px 5px;">Mở chạy tự động</a>');
    const $btn2 = $('<a class="btnextaction" href="javascript:;" style="margin: 10px 5px;">Xem lại log cũ</a>');
    const $btn3 = $('<a class="btnextaction" href="javascript:;" style="margin: 10px 5px;">Ghi file demo</a>');
    const btnwrap = document.createElement("DIV");
    btnwrap.className = "ext-btnwrap";

    const autoPanel = extAutoPlayStatusPanel(false);
    
    $btn.appendTo(btnwrap);
    $btn2.appendTo(btnwrap);
    // $btn3.appendTo(btnwrap);
    $(btnwrap).appendTo(wrap);
    $(autoPanel).appendTo($(wrap));

    
    $btn.on('click', async function(){
        
        let $this = $(this);
        if($this.hasClass('loading')) return;
        chrome.runtime.sendMessage({CallAction: "TOGGLE_AUTO"}, function(response) {
            const data = response.data;
            console.log(data);
            doAutoPassData($this, autoPanel, data.isAuto);
        });
    });
    $btn2.on('click', async function(){
        $(autoPanel).html('Bắt đầu lấy file logs');
        await sendMessagePromise({CallAction: "TURNOFF_AUTO"});
        $(autoPanel).html('');
        
        try {
            
            const res = await sendMessagePromise({
                CallAction: "mid_getAllLogs", 
                data: ''
            });
            const resData = res.data;
            if(resData.status){
                //success
                const filelist =  resData.data;
                console.log(filelist);
                filelist.forEach(item => {
                    const $htmlElement = $(`<p><a href="javascript:;">${item}</a></p>`);
                    $(autoPanel).prepend($htmlElement);
                    $htmlElement.on('click', 'a', async () => {
                       
                        $(autoPanel).html('Lấy nội dung File ....');
                        const resData =  await readJsonFileLog(item);
                        $(autoPanel).html('');
                        console.log(resData);
                        const keys = Object.keys(resData);
                        keys.forEach(item => {
                            if(item == "total"){
                                const offsetCount = parseInt(resData[item])  - (keys.length - 1);
                                $(autoPanel).prepend(`<p>Tổng cộng có ${resData[item]} đơn. Đã quét ${ keys.length - 1 }. Chưa quét ${offsetCount} đơn</p>`);
                            } else {
                                const {url, ishl, logs} = resData[item];
                                // const newlogs = logs.replace(item.toString() , `<a href="${ url }" target="_blank">${ item }</a>`);

                                $(autoPanel).prepend(logs);
                            }
                            
                        });

                    });
                });

            } else {
                if(resData.error === "Failed to fetch") resData.message = "Chưa mở ứng dụng lấy dữ liệu đơn hàng";
                Swal.fire({
                    title: '',
                    type: 'error',
                    html: `<p>${resData.message}</p>`
                });
            }
        } catch (error) {
            console.log(error);
        }
    });
    $btn3.on('click', async function(){
     
        appendLogFile('aaa', {
            aa: 's111ss',
            ssss: 'ssss'
        });
    });
    chrome.runtime.sendMessage({CallAction: "GET_EXTENTION_STATUS"}, async function(response) {
        const extStatus = response.data;
        console.log(extStatus);
        if(extStatus.isAuto){
            await sendMessagePromise({CallAction: "TURNOFF_AUTO"});
            $btn.removeClass('loading');
        } else {
            $btn.removeClass('loading');
        }
        // sequenceControler({...extStatus,...{list: pageData}}, wrap);
    });
    
}


function extAutoPlayStatusPanel(isOpen){
    const htmlmakup = `<div class="autoplaystatus">
    </div>`;
    const div = document.createElement('div');
    div.innerHTML = htmlmakup.trim();
    return div.firstChild;
}
function injectExtWrap(wrap, tt = EXT_TITLE){

    wrap.setAttribute('class', 'extnh-wrap');
    $(wrap).html('');
    let titleH = document.createElement('H4');
    titleH.setAttribute('class', 'list-tt');
    titleH.innerHTML = tt + " - " + '<a href="https://websitenhaphang.com" target="_blank">websitenhaphang.com</a>';
    $(titleH).prependTo(wrap);
}
function createDataHTML(arrayData){
    if(arrayData.length > 0){
        let itUL = document.createElement("UL");
        itUL.setAttribute('class', 'pdList-ul');
        const cpIocnURI = chrome.runtime.getURL('images/clippy.svg');
        
        arrayData.forEach((element, index) => {
            let status = $('<span class="order_status"></span>'); 
            let stringURL =  element.link;
            let newP = document.createElement("LI");
            let newLinkElment = document.createElement("A");
            let orderCodeSpan = document.createElement("SPAN");
            let copyCodeSpan = document.createElement("SPAN");
            $(copyCodeSpan).html(`<img style="width: 14px" src="${ cpIocnURI }"> `);
            
            orderCodeSpan.setAttribute('class', 'orderid');
            $(orderCodeSpan).html(element.codeOrder);
            $(newLinkElment).html(`<span class="orderpr">${element.amount}</span>`);
            $(orderCodeSpan).prependTo($(newLinkElment));
            $(copyCodeSpan).appendTo($(orderCodeSpan));
            newLinkElment.setAttribute('href', stringURL);
            newLinkElment.setAttribute('target', '_blank');
            $(newLinkElment).appendTo($(newP));
            newP.setAttribute('data-order-id', element.codeOrder);
            $(newP).appendTo(itUL);
            status.attr('title', element.status);
            let ssHTMl = '<i style="color: blue">Shop đã phát hàng</i>';
            switch (element.status) {
                case "交易成功":
                    ssHTMl = '<i style="color:green">Shop đã chuyển xong</i>';
                    break;
                case "交易关闭":
                    ssHTMl = '<i style="color:gray">Giao dịch đã kết thúc</i>';
                    break;
                case "买家已付款":
                case "等待卖家发货":
                    ssHTMl =  '<i style="color: gold">Chờ Shop phát hàng</i>';
                    element.isLogistic = false;
                    break;
                case "等待买家确认收货":
                    ssHTMl = '<i style="color:#52b336">Chờ xác nhận</i>';
                    break;
                case "等待买家付款":
                case  "":
                    ssHTMl = '<i style="color: red">Chờ thanh toán</i>';
                default:

                    break;
            };
            status.html(ssHTMl);

            $(newP).find('a').append(status);
            element.html = newP;
            const cb = new ClipboardJS(copyCodeSpan);
            $(copyCodeSpan).on('click', function(e){
                e.preventDefault();
                cb.text = function(trigger){
                    console.info('đã copy order Code:' + element.codeOrder);
                    let lb = $('<span class="ttip">copied</span>')
                    $(trigger).append(lb);
                    setTimeout(function(){
                        lb.fadeOut(function(){lb.remove()});
                    }, 700);
                    return element.codeOrder;
                }
                
            });
            copyCodeSpan.click();
            // new ClipboardJS(orderCodeSpan, {
            //     text: function(trigger) {
            //         alert('đã copy order Code');
            //         return $(trigger).html();
            //     }
            // });

        });
        
        return itUL;
    } else {
        return false;
    }
}
function inject1688L_HTML(wrap){
    $(wrap).html('');
    injectExtWrap(wrap);
    let pageData = datadingdan1688();
    console.log(pageData);
    let list = createDataHTML(pageData);
    $(list).appendTo(wrap);
    const $btn = $('<a class="btnextaction" href="javascript:;" style="margin: 10px 5px;">Quét mã</a>');
    const $btn2 = $('<a class="btnextaction" href="javascript:;" style="margin: 10px 5px;">Lấy thông tin vận đơn</a>');
    const $btn3 = $('<a class="btnextaction btntoggleauto" href="javascript:;" style="margin: 10px 5px;">Mở chạy tự động</a>');
    const btnwrap = document.createElement("DIV");
    btnwrap.className = "ext-btnwrap";
    $btn.appendTo(btnwrap);
    $btn2.appendTo(btnwrap);
    $btn3.appendTo(btnwrap);
    $(btnwrap).appendTo(wrap);
    let note = $('<p><i>Bấm vào icon để copy code</i> <br>Lưu ý đối với trang này(1688): chỉ hiện những đơn hàng đang active</p>');
    note.insertAfter(list);
    
    // checkAvailableOrders(pageData, wrap);
    $btn.on('click', function(){
        checkAvailableOrders(pageData, wrap);
    });
    $btn2.on('click', async function(){
        if(!$btn2.hasClass("loading")){
            $btn2.addClass('loading');
            await trackingOrders(pageData, wrap);
            $btn2.removeClass('loading');
        } else {
            Swal.fire({
                title: '',
                type: 'error',
                html: `<p>Đang tiến hàng tiến trình lấy mã vận đơn.</p>`
            });
        }
            
    });
    $btn3.on('click', async function(){
        let $this = $(this);
        chrome.runtime.sendMessage({CallAction: "TOGGLE_AUTO"}, function(response) {
            const data = response.data;
            console.log(data);
            if(data.isAuto){
                $this.html('Tắt chạy tự động');
                sequenceControler({...data,...{list: pageData}}, wrap);
            } else {
                $this.html('Mở chạy tự động');
                sequenceControler({...data,...{list: pageData}}, wrap);
            }
        });
    });
    chrome.runtime.sendMessage({CallAction: "GET_EXTENTION_STATUS"}, function(response) {
        const extStatus = response.data;
        console.log(extStatus);
        sequenceControler({...extStatus,...{list: pageData}}, wrap);
    });
}

async function checkAvailableOrders(listod, wrap){
    
    const request = listod.map(function(item){
        return { "MainOrderCode": item.codeOrder}
    });
    
    console.log(request);
    

    if ($(wrap).find('.ajaxload').length > 0) $(wrap).find('.ajaxload').remove();
    
    try {
        if($(wrap).hasClass('loadingcont')){
            return false;
        };
        $(wrap).addClass('loadingcont');
        var note = $('<span class="ajaxload">Tiến hành quét mã đơn.....</span>');
        note.appendTo(wrap);
        const {data} = await sendMessagePromise({CallAction: "checkorderList", data: request});
        $(wrap).removeClass('loadingcont');
        if(data.Status !== "Success"){
            throw data.responseText;
        }
        const result = data.ListMain;
        const newrr = listod.map(function(item){
            let isValue = result.find(rsi => rsi.MainOrderCode === item.codeOrder);
            return {...item, ...isValue};
        });
        newrr.forEach(function(item){
            const SScolorStyle = ['red', 'gold', 'green'];
            let ssIndex = 0;
            switch (item.Status) {
                case 1:
                    if ($(item.html).find('a').find('.clstatus').length > 0) $(item.html).find('a').find('.clstatus').remove();
                    ssIndex = 0;
                    break;
                case 2:
                    if ($(item.html).find('a').find('.clstatus').length > 0) $(item.html).find('a').find('.clstatus').remove();
                    ssIndex = 2;
                    break;
                case 3:
                    if ($(item.html).find('a').find('.clstatus').length > 0) $(item.html).find('a').find('.clstatus').remove();
                    ssIndex = 1;
                    $(item.html).addClass('getable');
                    break;
                case 4:
                    if ($(item.html).find('a').find('.clstatus').length > 0) $(item.html).find('a').find('.clstatus').remove();
                    ssIndex = 1;
                    $(item.html).addClass('getable');
                    break;
                default:
                    console.log('undefine Status', item.html);
                    break;
            }
            $(item.html).find('a').prepend('<span class="clstatus" style="color:'+SScolorStyle[ssIndex]+'">' + item.StatusString + '</span> ');

        });

        note.html('quét mã hoàn tất');
        setTimeout(() => {
            note.fadeOut(function(){$(this).remove()});
        }, 1000);
        return true;

    } catch (error) {
        $(wrap).removeClass('loadingcont');
        note.html('Đã xảy ra lỗi trong quá trình quét !!!! vui lòng thử lại sau ít phút');
        console.log(error);
        return false;
    }

}


function dingdan_array() {
    var date = "";
    var codeOrder = "";
    var status = "";
    var name_shop = "";
    var web = "";
    var notechietkhau = "";
    var arrayData = [];
    $('.js-order-container').each(function (index, element) {
        var table =  $(this).find('table');
        var linkLogistic = table.find('#viewLogistic');
        table.find('tbody').each(function (index, element) {
            if(table.find('tbody').length === 3 ){
                if(index == 2 && status == ""){
                    status = $(element).find('tr').eq(1).find('td').eq(5).find('div').find('p').find('span').text();  
                 
                }
            } else {
                if (index == 1){
                    //   console.log(element);
                    //   shop =  element.('.production-mod__pic___2xUhO');
                    web =  $(this).find('tr').find('td').find('div').find('div').html();
                    web =  web.split("&amp");
                    web = web[0];
                    web =  web.split("id=");
                    web = web[1];
                    $(this).find('tr').find('td').each(function (index , value) {
                        if (index == 5){
                            status = $(this).find('div').find('p').find('span').text();
                        }
                    });
                }
            }
            
            
            
            if($(this).attr('class') == 'bought-wrapper-mod__head___2vnqo'){

                notechietkhau =  $(this).find('.chietkhau1688').val();

                $(this).find('tr').find('td').each(function (index, element) {
                    if(index == 0)
                    {
                        //    console.log(element)
                        var arr = $(this).text().split("订单号: ");
                        date = arr[0];
                        codeOrder = arr[1]
                    }
                    else
                    if(index == 1)
                    {
                        // console.log(web);
                        name_shop =$(this).text();
                        return false
                    }
                })
            } else {
                // var value =   $(this).find('tr').index(0).find('td').index(4).text()
                
                $(this).find('tr').each(function (index, element) {
                    if(index == 0)
                    {
                        var linkDetail = $(this).find('td').eq(5).find('#viewDetail');
                        
                        

                        $(this).find('td').each(function (index, element)     {
                            
                            if(index == 4){

                                $(this).find('div').find('div').find('p').find('strong').find('span').each(function (index, element) {
                                    if(index == 1)
                                    {
                                        var data = {};
                                        data["date"] = date;
                                        data["codeOrder"] = codeOrder;
                                        data["name_shop"] = name_shop;
                                        data["amount"] =  $(this).text();
                                        data["idsp"] = web;
                                        data["status"] = status;
                                        data["note"] = notechietkhau;
                                        data["link"] = linkDetail.attr('href');
                                        data["isLogistic"] = linkLogistic.length > 0;
                                        arrayData.push(data)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    });
    return arrayData
}

function datadingdan1688() {
    var arrayData = [];
    $('.item-active').each(function (index, element) {
        var Data = {};
        var table =  $(this).find('div').find('div').find('div').find('.col-group');
        date = table.find('.date').text();
        date = date.split(" ");
        date = date[0];
        Data['date'] = date;
        order = table.find('.order-id').text();

        order = order.split("订单号：");
        order = order[1];
        //   console.log(order)
        Data['codeOrder'] = order;
        Data['shopname'] = table.find('.bannerCorp').attr('data-copytitle');
        //  console.log(shopname)
        infor = $(this).find('div').find('div').find('table').find('tbody').find('tr');
        amount = infor.find('.total').text();
        amount = infor.find('.total').text();
        amount = amount.trim();
        Data['amount'] = amount;
        //    console.log(amount)
        status = infor.find('.s7').text();
        status = status.trim();
        status = status.slice(0,10);
        status = status.trim();
        note = $(this).find('.chietkhau1688').val();
        Data['note'] = note;
        Data['status'] = status.trim();
        Data['isLogistic'] = infor.find('.s7').find('.logistics-detail-link').length > 0;
        Data['link'] = infor.find('.s7').find('.bannerOrderDetail').attr('href');
        arrayData.push(Data)
    });
    return arrayData
}

