'use strict';
console.log('Open ext popup');

const loginForm = document.querySelector("#extPopLogin");
const WRAP = loginForm.parentElement;
const reloginlink = document.createElement("A");
reloginlink.innerHTML = "Đăng nhập lại";
reloginlink.className = "btn submit-btn primary-btn";
reloginlink.addEventListener('click', ()=>{
    $(loginForm).show();
    $(reloginlink).hide();
});

const softValidate = ({username, password}) => {
    console.log(username, password);
}
const loginResponseHandle = (response) => {
    console.log(response);

    if(response.handlerReceive){
        const data = response.data;
        if(data.hasOwnProperty("error")){
            $(WRAP).find("#fstatus").html(data.error.message);
            if(data.error.name === "TimeoutError")  $(WRAP).find("#fstatus").html("Thời gian phản hồi quá lâu. Vui lòng thử lại");
            if(data.error === "Failed to fetch")  $(WRAP).find("#fstatus").html("Không thể kết nối với máy chủ local vui lòng kiểm tra lại");
        } else {
            if(data.status) $(WRAP).find("#fstatus").html("Xin chào: "+ data.data.username);
            console.log(data);
            $(loginForm).hide();
            $(WRAP).append(reloginlink);
        }
    } else {

    }
}
chrome.runtime.sendMessage({CallAction: "GET_EXTENTION_STATUS"}, function(response) {
    const extStatus = response.data;
    console.log(extStatus);
    if(extStatus.isMidLogin) {
        
        $(WRAP).find("#fstatus").html("Xin chào: "+ extStatus.userName);
        $(loginForm).hide();    
        $(WRAP).append(reloginlink);
    }
    
});

loginForm.addEventListener("submit", (event)=>{
    event.preventDefault();
    let fDataArray = (new FormData(event.target)).entries();
    let fData = {};
    fDataArray = Array.from(fDataArray);
    fDataArray.map(item => {
        fData[item[0]] = item[1];
    });
    softValidate(fData);
    $(WRAP).find("#fstatus").html("Bắt đầu login. Quá trình này có thể mất từ 8 - 20s. Vui lòng không tắt cửa sổ này trong quá trình xử lý.");
    chrome.runtime.sendMessage({CallAction: "mid_login", data: fData}, function(response) {
        $(loginForm).find('input:not([type="submit"])').val("");
        loginResponseHandle(response);
    });
});



 