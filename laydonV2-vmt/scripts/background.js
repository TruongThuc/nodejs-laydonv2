const API_HOST = 'https://extension.monamedia.net/webservice1.asmx';
const API_M_DEV = "http://localhost:3000";
// const API_M_PRO = "http://extentions.monamedia.net";
const API_M = API_M_DEV;
const EXTENTION_STATUS = {
    isAuto: false,
    isMidLogin : false,
    countGetStatus: 0,
    userName: ""
}
const requestHandle = {
    TOGGLE_AUTO:  (request, respone) =>{
        EXTENTION_STATUS.isAuto = !EXTENTION_STATUS.isAuto;
        respone(EXTENTION_STATUS);
    },
    TURNOFF_AUTO:  (request, respone) =>{
        EXTENTION_STATUS.isAuto = false;
        respone(EXTENTION_STATUS);
    },
    TURNON_AUTO:  (request, respone) =>{
        EXTENTION_STATUS.isAuto = true;
        respone(EXTENTION_STATUS);
    },
    GET_EXTENTION_STATUS: (request, respone) =>{
        if(EXTENTION_STATUS.userName){
            EXTENTION_STATUS.countGetStatus++;
        }
        respone(EXTENTION_STATUS);
    },
    checkorderList:  (request, respone) => {
        const listdata  = JSON.stringify(request);
        $.ajax({
            url: API_HOST+'/CheckList',
            method: 'POST',
            data: {
                ListMainOrder : listdata
            }
        }).done(function( msg ){
            respone(msg);
        }).fail(function( jqXHR, textStatus ) {
            console.log("Request failed: " , textStatus);
            console.log(jqXHR);
            respone(jqXHR);
        })
        
    },
    updateOrder : (request, respone) => {
        //(request.odCode, request.data, request.type)
        const value = JSON.stringify(request.data);
        $.ajax({
            url: API_HOST+'/receivesmallpackage',
            method: 'POST',
            data: {
                MainOrderCode: request.odCode,
                Value: encodeURI(value),
                Type: request.type
            }
        }).done(function( msg ){
            respone(msg);
        }).fail(function( jqXHR, textStatus ) {
            console.log("Request failed: " , textStatus);
            console.log(jqXHR);
            respone(jqXHR);
        })
    },
    getOrderListFull : (request, respone) => {
        //(request.odCode, request.data, request.type)
        const value = JSON.stringify(request.data);
        $.ajax({
            url: API_HOST+'/GetListMainOrderCode',
            method: 'POST',
            data: {
                // MainOrderCode: request.odCode,
                // Value: encodeURI(value),
                // Type: request.type
            }
        }).done(function( msg ){
            respone(msg);
        }).fail(function( jqXHR, textStatus ) {
            console.log("Request failed: " , textStatus);
            console.log(jqXHR);
            respone(jqXHR);
        })
    },
    mid_login: async function(request, respone){
        try {
            const res = await fetch(API_M + '/tblogin', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                body: JSON.stringify({username: request.username,
                    password: request.password}),
            });
            const result = await res.json();
            if(result.hasOwnProperty("error")){
                throw result;
            } else {
                EXTENTION_STATUS.isMidLogin = true;
                EXTENTION_STATUS.countGetStatus = 0;
                EXTENTION_STATUS.userName = result.data.username;
                respone({...result,...EXTENTION_STATUS});
            }
            
        } catch (error) {
            respone(error);
        }
    },
    mid_fetchdetail: async (request, respone) => {
        try {
            const res = await fetch(API_M + '/tbfetchdetail', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                body: JSON.stringify({detailurl: request.detailurl}),
            });
            const result = await res.json();
            console.log(result);
            if(result.hasOwnProperty("error")){
                throw result;
            } else {
                
                respone(result);
                // requestHandle["updateOrder"](request.oId, result.data, request.type);
            }
            
        } catch (error) {
            respone(error);
        }
    },
    mid_readlog:  async (request, respone) => {
        try {
            const res = await fetch(API_M + '/readlog', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                body: JSON.stringify({fileName: request}),
            });
            const result = await res.json();
            console.log(result);
            respone(result);
        } catch (error) {
            respone(error);
        }
    },
    mid_writelog:  async (request, respone) => {
        const {fileName, jsonData} = request;
        try {
            const res = await fetch(API_M + '/writelog', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                
                body: JSON.stringify({fileName: fileName, jsonData: JSON.stringify(jsonData)}),
            });
            const result = await res.json();
            console.log(result);
            respone(result);
        } catch (error) {
            
        }
    },
    mid_updatelog:  async (request, respone) => {
        const {fileName, jsonData} = request;
        try {
            const res = await fetch(API_M + '/updatelog', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                
                body: JSON.stringify({fileName: fileName, jsonData: JSON.stringify(jsonData)}),
            });
            const result = await res.json();
            console.log(result);
            respone(result);
        } catch (error) {
            
        }
    },
    mid_getAllLogs: async (request, respone) => {
        
        try {
            const res = await fetch(API_M + '/readalllog', {
                method: "POST", 
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8' 
                },
                // body: ,
            });
            const result = await res.json();
            console.log(result);
            respone(result);
        } catch (error) {
            
            respone(error);
        }
    }
}

chrome.runtime.onInstalled.addListener(function(){
    console.log('ext Installed.');
  
});
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    console.log(sender.tab ?
                "from a content script:" + sender.tab.url :
                "from the extension");
    if (request.UpdateDetail == "Done"){
        sendResponse({handlerReceive: "success"});
        chrome.tabs.query({currentWindow: true, url: [ "http://*.buyertrade.taobao.com/*", "*://buyertrade.taobao.com/*", 
        "https://trade.taobao.com/trade/detail**", "https://trade.tmall.com/detail**", 
        "*://trade.1688.com/order/buyer_order_list.htm*", "*://trade.1688.com/order/new_step_order_detail.htm*",
        "*://wuliu.1688.com/order", "*://work.1688.com/home/buyer.htm**"]}, function(tabs) {
            console.log(tabs);
            chrome.tabs.sendMessage(tabs[0].id, {action: "UpdateListItem", orderId: request.orderId}, function(response) {
                console.log(response);
            });
        });
    }


    if (request.CallAction){
        requestHandle[request.CallAction](request.data, function(result){
            if(result instanceof Error) result = {error: result.message};
            sendResponse({handlerReceive: request.CallAction , data: result});
        });
        
    }


    return true; // return when this message is a async message
});


